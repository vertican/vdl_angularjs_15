﻿
var gulp = require('gulp');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var print = require('gulp-print');
var del = require('del');

var paths = {
    bower: "./bower_components/",
    lib: "./lib/"
};

var bower = {
    "angular": "angular/angular.min*.{js,map}",
    "angular-route": "angular-route/angular-route.min*.{js,map}"
}

gulp.task('clean:library', function () {
    return del([
        // clean everything inside the lib folder using a globbing pattern
      'lib/*'
    ]);
});

gulp.task("copy:libraries", ['clean:library'], function () {

    for (var destinationDir in bower) {
        gulp.src(paths.bower + bower[destinationDir])
            .pipe(gulp.dest(paths.lib + destinationDir));
    }
});

gulp.task("inject:index", function () {


    var target = gulp.src('./index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths: 
    var sources = gulp.src(['./lib/**/*.js', './src/**/*.css'], { read: false });

    return target.pipe(inject(sources, { relative: true }))
      .pipe(gulp.dest('./'));

    //for (var destinationDir in bower) {
    //    gulp.src("index.html")
    //        .pipe(inject(
    //        gulp.src(paths.lib + destinationDir,
    //            { read: false }), { relative: true }))
    //    .pipe(gulp.dest('./'));
    //}

    //return gulp.src('index.html')
    //    .pipe(inject(
    //        gulp.src(paths.javascript,
    //            { read: false }), { relative: true }))
    //    .pipe(gulp.dest('./'));
});